import multer from "multer";
import { v4 as uuidv4 } from "uuid";
import path from "path";
import { global } from "../config/constant";

const storage = multer.diskStorage({
  destination: path.join(__dirname, global.rutaImagenes),
  filename: (req, file, cb) => {
    cb(null, uuidv4() + path.extname(file.originalname));
  },
});

const uploadImagen = multer({
  storage: storage,
}).single(global.simpleImage);
export default uploadImagen;
