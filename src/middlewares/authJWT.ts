import { Response, NextFunction } from "express";
import { decodedToken } from "../utils/jwt";
import authService from "../services/auth.service";

const authSvr = new authService();
export const verifyToken = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  try {
    let token = req.headers["x-access-token"];

    if (!token)
      return res.status(403).json({ status: 1, msj: "ingrese token" });

    const decoded: any = decodedToken(token);

    const usuario = await authSvr.getUsuarioForID(decoded.id);

    req.usuarioID = usuario.id;
    next();
  } catch (error) {
    return res.status(401).json({ status: 1, message: "No autorizado" });
  }
};

export const verifyRolAdmin = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  try {
    const roles: any = await authSvr.getUsuarioRoles(req.usuarioID);

    for (const rol of roles) {
      if (rol.descripcion === "admin") {
        next();
        return;
      }
    }

    return res.status(403).json({ status: 1, msj: "Debe ser Admin" });
  } catch (error) {
    return res.status(401).json({ status: 1, message: error });
  }
};
