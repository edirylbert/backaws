import { Router } from "express";
import * as presentacionCtrl from "../../controllers/presentacion/presentacion.controller";

const presentacionRouter = Router();

presentacionRouter.get("/", presentacionCtrl.getPresentaciones);
presentacionRouter.get("/:id", presentacionCtrl.getPresentacion);
presentacionRouter.post("/", presentacionCtrl.createPresentacion);
presentacionRouter.put("/:id", presentacionCtrl.editPresentacion);
presentacionRouter.delete("/:id", presentacionCtrl.deletePresentacion);

export default presentacionRouter;
