import { Router } from "express";
import * as authCtrl from "../../controllers/auth/auth.controller";
import { verifyToken, verifyRolAdmin } from "../../middlewares/authJWT";
const authRouter = Router();

authRouter.post("/login", authCtrl.login);

authRouter.post("/register", authCtrl.register);

authRouter.get("/token", authCtrl.verificarToken);

export default authRouter;
