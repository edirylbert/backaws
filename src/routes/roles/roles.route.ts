import { Router } from "express";
import * as rolCtrl from "../../controllers/roles/rol.controller";

const rolRouter = Router();

rolRouter.get("/", rolCtrl.getRoles);
rolRouter.get("/:id", rolCtrl.getRol);
rolRouter.post("/", rolCtrl.createtRol);
rolRouter.put("/:id", rolCtrl.editRol);
rolRouter.delete("/:id", rolCtrl.deleteRol);

export default rolRouter;
