import { Router } from "express";
import * as clienteCtrl from "../../controllers/clientes/cliente.controller";
import { verifyToken, verifyRolAdmin } from "../../middlewares/authJWT";
const clienteRouter = Router();

clienteRouter.get("/", clienteCtrl.getClientes);
clienteRouter.get("/:id", clienteCtrl.getCliente);
clienteRouter.post(
  "/",
  [verifyToken, verifyRolAdmin],
  clienteCtrl.createtCliente
);
clienteRouter.put("/:id", clienteCtrl.editCliente);
clienteRouter.delete("/:id", clienteCtrl.deleteCliente);

export default clienteRouter;
