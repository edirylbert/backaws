import { Router } from "express";
import * as productoCtrl from "../../controllers/producto/producto.controller";
import { verifyToken, verifyRolAdmin } from "../../middlewares/authJWT";
import uploadImagen from "../../middlewares/multer";

const productoRouter = Router();

productoRouter.get("/", productoCtrl.getProductos);
productoRouter.get("/:id", productoCtrl.getProducto);
productoRouter.get("/stock/bajo", productoCtrl.getStockBasjoProductos);
//productoRouter.post("/", uploadImagen, productoCtrl.createtProducto);
productoRouter.post("/", productoCtrl.createtProducto);
//productoRouter.put("/:id", uploadImagen, productoCtrl.editProducto);
productoRouter.put("/:id", productoCtrl.editProducto);
productoRouter.delete("/:id", productoCtrl.deleteProducto);
productoRouter.post("/create", productoCtrl.createProducto);
productoRouter.delete(
  "/eliminar/presentacion/:idproducto/:idpresentacion",
  productoCtrl.deletePresentacionProducto
);

export default productoRouter;
