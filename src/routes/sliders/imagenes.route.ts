import { Router } from "express";

import * as imagenCtrl from "../../controllers/sliders/imagen.controller";
const imageneRouter = Router();

imageneRouter.get("/", imagenCtrl.getImagenesSlider);
imageneRouter.get("/:imagen", imagenCtrl.getImagen);

export default imageneRouter;
