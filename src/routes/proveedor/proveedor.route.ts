import { Router } from "express";
import * as proveedorCtrl from "../../controllers/proveedor/proveedor.controller";
import { verifyToken, verifyRolAdmin } from "../../middlewares/authJWT";

const proveedorRouter = Router();

proveedorRouter.get("/", proveedorCtrl.getProveedores);
proveedorRouter.get("/:id", proveedorCtrl.getProveedor);
// proveedorRouter.post(
//   "/",
//   [verifyToken, verifyRolAdmin],
//   proveedorCtrl.createProveedor
// );
proveedorRouter.post(
  "/",
  [verifyToken, verifyRolAdmin],
  proveedorCtrl.createProveedor
);
proveedorRouter.put("/:id", proveedorCtrl.editProveedor);
proveedorRouter.delete("/:id", proveedorCtrl.deleteProveedor);
proveedorRouter.delete(
  "/eliminar/producto/:idProveedor/:idProducto",
  proveedorCtrl.deleteProductoProveedor
);

export default proveedorRouter;
