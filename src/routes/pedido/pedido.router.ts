import { Router } from "express";
import * as pedidoCtrl from "../../controllers/pedido/pedido.controller";

const pedidoRouter = Router();

pedidoRouter.get("/", pedidoCtrl.getPedidos);
pedidoRouter.get("/:id", pedidoCtrl.getPedido);
pedidoRouter.get("/pendientes/del-dia", pedidoCtrl.pedidosPendientes);
pedidoRouter.get("/despachados/del-dia", pedidoCtrl.pedidosDespachados);
pedidoRouter.get("/con/rotura/stock", pedidoCtrl.pedidosRoturaStock);
pedidoRouter.post("/", pedidoCtrl.createPedido);
pedidoRouter.put("/:id", pedidoCtrl.editPedido);
pedidoRouter.put("/despachar/:idPedido", pedidoCtrl.despacharPedido);
pedidoRouter.delete("/:id", pedidoCtrl.deletePedido);
pedidoRouter.delete(
  "/eliminar/producto/:idpedido/:idproducto/:idpresentacion",
  pedidoCtrl.deleteProductoPedido
);

export default pedidoRouter;
