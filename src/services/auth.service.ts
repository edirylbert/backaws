import { connectBD } from "../db/connectBD";
import { Pool, QueryResult, QueryParse } from "pg";
import { Usuario } from "../interfaces/usuarios.interface";
class authService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }

  async createUsuario(usuario: Usuario) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__usuarios_registrar_usuarios($1,$2,$3,$4) as res",
      [usuario.username, usuario.email, usuario.password, usuario.roles]
    );

    return response.rows[0].res;
  }
  async getUsuarioForEmail(email: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT
        u.id_usuario AS ID,
        u.username_usuario AS username,
        u.password_usuario AS password,
        u.email_usuario AS email,
        created_on AS create
      FROM usuario u 
      WHERE u.email_usuario = $1`,
      [email]
    );
    if (response.rowCount === 0) return false;
    return response.rows[0];
  }

  async getUsuarioForID(ID: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT
        u.id_usuario AS ID,
        u.username_usuario AS username,
        u.password_usuario AS password,
        u.email_usuario AS email,
        u.created_on AS create
      FROM usuario u 
      WHERE u.id_usuario = $1`,
      [ID]
    );
    if (response.rowCount === 0) return false;
    return response.rows[0];
  }
  async getUsuarioRoles(ID: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT 
        uxr.id_rol AS rol,
        r.descripcion_rol AS descripcion
        FROM usuario u,
          usuario_x_rol uxr,
          rol r
        WHERE u.id_usuario = uxr.id_usuario
        AND   uxr.id_rol   = r.id_rol
        AND u.id_usuario   = $1
      `,
      [ID]
    );
    if (response.rowCount === 0) return false;

    return response.rows;
  }
}

export default authService;
