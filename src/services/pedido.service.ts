import { connectBD } from "../db/connectBD";
import { Pool, QueryResult } from "pg";
import { Pedido } from "../interfaces/pedido.interface";

class PedidoService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }
  async getPedidos() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT
        p.id_pedido  AS id,
        p.nom_pedido AS nombre,
        p.ape_pedido AS apellido,
        p.created_on AS fecha
        FROM pedido p
        WHERE p.estado_pedido = 0`
    );

    return response.rows;
  }
  async getPedido(id: number) {
    const response: QueryResult = await this.connectBD.query(
      `
      SELECT 
             JSONB_BUILD_OBJECT(
                 'id'            ,pe.id_pedido,
                 'nombres'       ,pe.nom_pedido,
                 'apellidos'     ,pe.ape_pedido,
                 'direccion'     ,pe.direc_pedido,
                 'delivery'      ,pe.delivery_pedido,
                 'tipopedido'    ,pe.tipo_pedido,
                 'telefono'      ,pe.tel_pedido,
                 'total'         ,pe.total_pedido,
                 'productos' 	   ,(SELECT JSONB_AGG(
                                        JSONB_BUILD_OBJECT(
                                          'idproducto'		  ,pro.id_producto,
                                          'nombre' 		      ,pro.nom_producto,
                                          'cantidad'	      ,pxp.cantidad,
                                          'presentacion'	  ,pre.descrip_presentacion,
                                          'idpresentacion'	,pxp.id_presentacion
                                        ))
                                    FROM  producto_x_pedido pxp,
                                          producto pro,
                                          presentacion pre
                                    WHERE pxp.id_pedido = $1
                                    AND   pxp.id_producto = pro.id_producto
                                    AND   pxp.id_presentacion = pre.id_presentacion
                                    ) 
              )AS res
         FROM pedido pe
         WHERE pe.id_pedido = $1
         AND   pe.estado_pedido = 0
     `,
      [id]
    );

    return response.rows[0].res;
  }
  async createPedido(pedido: Pedido) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__pedidos_registrar_pedidos($1,$2,$3,$4,$5,$6,$7,$8) as res",
      [
        pedido.total,
        pedido.direccion,
        pedido.delivery,
        pedido.nombres,
        pedido.apellidos,
        pedido.tipoPedido,
        pedido.telefono,
        pedido.itemsPedidos,
      ]
    );
    return response.rows[0].res;
  }
  async editPedido() {
    return { msj: "editPedido" };
  }
  async deletePedido() {
    return { msj: "deletePedido" };
  }
  async deleteProductoPedido(
    idproducto: number,
    idpedido: number,
    idpresentacion: number
  ) {
    const response: QueryResult = await this.connectBD.query(
      `DELETE 
          FROM producto_x_pedido 
          WHERE id_producto 	  = $1
          AND   id_pedido 	    = $2
          AND   id_presentacion = $3`,
      [idproducto, idpedido, idpresentacion]
    );

    return response.rowCount;
  }
  async despacharPedido(idpedido: number) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__pedidos_despachar_pedidos($1) as res`,
      [idpedido]
    );

    return response.rows[0].res;
  }

  async pedidosPendientes() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT 
        COUNT(p.created_on::date)  AS "pendientes"
        FROM pedido p 
        WHERE p.estado_pedido    = 0 `
    );

    return response.rows[0].pendientes;
  }
  async pedidosDespachados() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT 
        COUNT(p.created_on::date)  AS "despachados"
        FROM pedido p 
        WHERE p.estado_pedido    = 1`
    );

    return response.rows[0].despachados;
  }
  async pedidosRoturaStock() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__pedidos_rotura_stock_pedidos() as res`
    );

    return response.rows[0].res;
  }
}

export default PedidoService;
