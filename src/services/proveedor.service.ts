import { connectBD } from "../db/connectBD";
import { Pool, QueryResult } from "pg";
import { Proveedor } from "../interfaces/proveedor.interface";

class ProveedorService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }
  async getProveedores() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT 
        	p.id_proveedor  AS id ,
          p.nom_proveedor  AS nombre ,
         	p.ape_proveedor  AS apellido ,
        	p.direc_proveedor  AS direccion ,
          p.correo_proveedor  AS correo ,
        	p.tel_proveedor  AS telefono
        FROM proveedor p 
        WHERE p.estado_proveedor = 1
        ;`
    );

    return response.rows;
  }
  async getProveedor(id: number) {
    const response: QueryResult = await this.connectBD.query(
      `
      SELECT 
        JSONB_BUILD_OBJECT(
          'id'            ,p.id_proveedor,
          'nombre'        ,p.nom_proveedor,
          'apellido'		  ,p.ape_proveedor,
          'direccion'   	,p.direc_proveedor,
          'correo'        ,p.correo_proveedor ,
          'telefono'      ,p.tel_proveedor,
          'productos'		  ,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
                                                'id'		    ,p.id_producto,
                                                'nombre'    ,p.nom_producto
                                            ))
                            FROM producto p ,  
                                 proveedor_x_producto pxp 
                            WHERE  p.id_producto = pxp.id_producto
                            AND    pxp.id_proveedor = $1
                            AND    p.estado_producto = 1
                            )   
        )AS res
      FROM proveedor p 
      WHERE p.estado_proveedor = 1
      AND p.id_proveedor = $1
        ;`,
      [id]
    );

    return response.rows[0].res;
  }
  async createProveedor(proveedor: Proveedor) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__proveedores_registrar_proveedores($1,$2,$3,$4,$5,$6,$7) as res",
      [
        proveedor.nombre,
        proveedor.apellido,
        proveedor.direccion,
        proveedor.correo,
        proveedor.telefono,
        proveedor.idUsuario,
        proveedor.productos,
      ]
    );
    return response.rows[0].res;
  }
  async editProveedores(proveedor: Proveedor) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__proveedores_actualizar_proveedores($1,$2,$3,$4,$5,$6,$7) as res",
      [
        proveedor.id,
        proveedor.nombre,
        proveedor.apellido,
        proveedor.direccion,
        proveedor.correo,
        proveedor.telefono,
        proveedor.productos,
      ]
    );
    return response.rows[0].res;
  }
  async deleteProveedores(id: number) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__proveedores_delete_proveedores($1) as res",
      [id]
    );
    return response.rows[0].res;
  }

  async deleteProductoProveedor(idProveedor: number, idProducto: number) {
    const response: QueryResult = await this.connectBD.query(
      `
      DELETE 
        FROM proveedor_x_producto 
        WHERE id_proveedor 	  = $1
        AND   id_producto     = $2
    `,
      [idProveedor, idProducto]
    );
    return response.rowCount;
  }
}

export default ProveedorService;
