import { connectBD } from "../db/connectBD";
import { Pool, QueryResult } from "pg";
import { Rol } from "../interfaces/rol.interface";

class rolService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }

  async getRole(id: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT  
        r.id_rol AS ID,
        r.descripcion_rol AS descripcion
        FROM rol r 
        WHERE r.id_rol = $1
        AND   r.estado_rol = 1`,
      [id]
    );
    return response.rows;
  }
  async getRoles() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT  
        r.id_rol AS ID,
        r.descripcion_rol AS descripcion
        FROM rol r 
        WHERE  r.estado_rol = 1`
    );
    return response.rows;
  }

  async createRol(rol: Rol) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__roles_registrar_rol($1) as res`,
      [rol.descripcion]
    );
    return response.rows[0].res;
  }

  async editRol(id: string, rol: Rol) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__roles_actualizar_roles($1,$2) as res`,
      [id, rol.descripcion]
    );
    return response.rows[0].res;
  }
  async deleteRol(id: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__roles_delete_roles($1) as res`,
      [id]
    );
    return response.rows[0].res;
  }
}

export default rolService;
