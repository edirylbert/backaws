import { connectBD } from "../db/connectBD";
import { Pool, QueryResult } from "pg";
import { Presentacion } from "../interfaces/presentacion.interface";

class rolService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }

  async getPresentaciones() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT  
          p.id_presentacion AS ID,
          p.descrip_presentacion AS descripcion
          FROM presentacion p 
          WHERE p.estado_presentacion = 1`
    );
    return response.rows;
  }
  async getPresentacion(id: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT  
          p.id_presentacion AS ID,
          p.descrip_presentacion AS descripcion
          FROM presentacion p 
          WHERE p.id_presentacion = $1
          AND   p.estado_presentacion = 1`,
      [id]
    );
    return response.rows;
  }

  async createPresentacion(presentacion: Presentacion) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__presentaciones_registrar_presentacion($1) as res`,
      [presentacion.descripcion]
    );
    return response.rows[0].res;
  }

  async editPresentacion(id: string, presentacion: Presentacion) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__presentaciones_actualizar_presentacion($1,$2) as res`,
      [id, presentacion.descripcion]
    );
    return response.rows[0].res;
  }
  async deletePresentacion(id: string) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__presentaciones_delete_presentaciones($1) as res`,
      [id]
    );
    return response.rows[0].res;
  }
}

export default rolService;
