import { connectBD } from "../db/connectBD";
import { Pool, QueryResult } from "pg";
import { Cliente } from "../interfaces/clientes.interface";

class ClienteService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }

  async getClientes() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT  
        c.id_cliente AS ID,
        c.nom_cliente AS nombres,
        c.ape_cliente AS apellidos,
        c.direc_cliente AS direccion,
        c.correo_cliente AS correo,
        c.tel_cliente AS telefono,
        c.empresa_cliente AS empresa
        FROM cliente c WHERE c.estado_cliente = 1`
    );

    return response.rows;
  }
  async getCliente(id: number) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT  
        c.id_cliente AS id,
        c.nom_cliente AS nombres,
        c.ape_cliente AS apellidos,
        c.direc_cliente AS direccion,
        c.correo_cliente AS correo,
        c.tel_cliente AS telefono,
        c.empresa_cliente AS empresa
        FROM cliente c WHERE c.estado_cliente = 1
        AND c.id_cliente = $1
      `,
      [id]
    );
    return response.rows;
  }
  async createCliente(cliente: Cliente) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__clientes_registrar_clientes($1,$2,$3,$4,$5,$6,$7) as res",
      [
        cliente.nombres,
        cliente.apellidos,
        cliente.direccion,
        cliente.correo,
        cliente.telefono,
        cliente.empresa,
        cliente.id_usuario,
      ]
    );
    return response.rows[0].res;
  }
  async editCliente(cliente: Cliente) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__clientes_actualizar_clientes($1,$2,$3,$4,$5,$6,$7) as res`,
      [
        cliente.id,
        cliente.nombres,
        cliente.apellidos,
        cliente.direccion,
        cliente.correo,
        cliente.telefono,
        cliente.empresa,
      ]
    );
    return response.rows[0].res;
  }
  async deleteCliente(id: number) {
    const response: QueryResult = await this.connectBD.query(
      `SELECT * FROM public.__clientes_delete_cliente($1) res`,
      [id]
    );
    return response.rows[0].res;
  }
}

export default ClienteService;
