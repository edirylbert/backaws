import { connectBD } from "../db/connectBD";
import { Pool, QueryResult } from "pg";
import { Producto } from "../interfaces/producto.interface";

class ProductoService {
  private connectBD: Pool;
  constructor() {
    this.connectBD = connectBD;
  }

  async getProductos() {
    const response: QueryResult = await this.connectBD.query(
      `SELECT
        p.id_producto      AS id,
        p.nom_producto     AS nombre,
        p.descrip_producto AS descripcion,
        p.img_producto     AS imagen,
        MIN(pxp.precio)    AS precio
        FROM producto p,
             producto_x_presentacion pxp
        WHERE p.id_producto = pxp.id_producto
        AND   p.estado_producto = 1
        GROUP BY p.id_producto`
    );

    return response.rows;
  }
  async getProducto(id: number) {
    const response: QueryResult = await this.connectBD.query(
      `
        SELECT 
          JSONB_BUILD_OBJECT(
              'id'            	,p.id_producto,
              'nombre'       	  ,p.nom_producto,
              'descripcion'   	,p.descrip_producto,
              'imagen'        	,p.img_producto,
              'cantidadMax'     ,p.canti_max_producto,
              'cantidadMin'     ,p.canti_min_producto,
              'stock'     		  ,p.stock,
              'presentaciones' 	,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
                                            'id'		      ,p.id_presentacion,
                                            'descripcion' ,p.descrip_presentacion,
                                            'precio'	    ,pxp.precio,
                                            'stock'	      ,pxp.stock
                                        ))
                                    FROM producto_x_presentacion pxp,
                                        presentacion p	
                                    WHERE pxp.id_presentacion = p.id_presentacion
                                    AND  pxp.id_producto	  = $1
                                  )
              ) AS res
          FROM producto p 
          WHERE p.id_producto = $1 
          AND p.estado_producto = 1
          
        `,
      [id]
    );

    return response.rows[0].res;
  }
  async createProducto(producto: Producto) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__productos_registrar_productos($1,$2,$3,$4,$5,$6,$7) as res",
      [
        producto.nombre,
        producto.descripcion,
        producto.cantidadMax,
        producto.cantidadMin,
        producto.stock,
        producto.imagen,
        producto.presentaciones,
      ]
    );
    return response.rows[0].res;
  }
  async editProducto(producto: Producto) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__productos_actualizar_productos($1,$2,$3,$4,$5,$6,$7,$8) as res",
      [
        producto.id,
        producto.nombre,
        producto.descripcion,
        producto.cantidadMax,
        producto.cantidadMin,
        producto.stock,
        producto.imagen,
        producto.presentaciones,
      ]
    );
    return response.rows[0].res;
  }
  async deleteProducto(id: number) {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__productos_delete_productos($1) as res",
      [id]
    );
    return response.rows[0].res;
  }
  async deletePresentacionProducto(idProducto: number, idPresentacion: number) {
    const response: QueryResult = await this.connectBD.query(
      `
        DELETE 
          FROM producto_x_presentacion 
          WHERE id_producto 	  = $1
          AND   id_presentacion = $2`,
      [idProducto, idPresentacion]
    );
    return response.rowCount;
  }

  async bajoStockProducto() {
    const response: QueryResult = await this.connectBD.query(
      "SELECT * FROM public.__productos_bajo_stock_productos() as res"
    );
    return response.rows[0].res;
  }
}

export default ProductoService;
