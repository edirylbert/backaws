import express from "express";
import cors from "cors";
import morgan from "morgan";
import helmet from "helmet";
import clienteRouter from "../routes/clientes/cliente.route";
import authRouter from "../routes/auth/auth.route";
import rolRouter from "../routes/roles/roles.route";
import presentacionRouter from "../routes/presentacion/presentacion.router";
import productoRouter from "../routes/producto/producto.router";
import proveedorRouter from "../routes/proveedor/proveedor.route";
import pedidoRouter from "../routes/pedido/pedido.router";
import imageneRouter from "../routes/sliders/imagenes.route";
class Server {
  private app: express.Application;
  private port: number;
  private apiPaths = {
    clientes: "/api/cliente",
    auth: "/api/auth",
    roles: "/api/rol",
    presentaciones: "/api/presentacion",
    productos: "/api/producto",
    proveedor: "/api/proveedor",
    pedido: "/api/pedido",
    slider: "/api/imagenes",
  };

  constructor() {
    this.app = express();
    this.port = 3000;

    //MIDDLEWARES
    this.middleware();
    // Definir RUTAS
    this.routes();
  }

  routes() {
    this.app.use(this.apiPaths.clientes, clienteRouter);
    this.app.use(this.apiPaths.auth, authRouter);
    this.app.use(this.apiPaths.roles, rolRouter);
    this.app.use(this.apiPaths.presentaciones, presentacionRouter);
    this.app.use(this.apiPaths.productos, productoRouter);
    this.app.use(this.apiPaths.proveedor, proveedorRouter);
    this.app.use(this.apiPaths.pedido, pedidoRouter);
    this.app.use(this.apiPaths.slider, imageneRouter);
  }

  middleware() {
    //LECTURA DEL BODY

    this.app.use(express.urlencoded({ limit: "50mb", extended: false }));
    this.app.use(express.json({ limit: "50mb" }));
    //CORS
    this.app.use(cors());

    //HELMET
    this.app.use(helmet());

    //MORGAN
    this.app.use(morgan("tiny"));
  }

  start() {
    this.app.listen(this.port, () => {
      console.log(`SERVIDOR CORRIENDO EN EL PUERTO ${this.port}`);
    });
  }
}

export default Server;
