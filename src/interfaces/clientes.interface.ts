export interface Cliente {
  id?: number;
  nombres: string;
  apellidos: string;
  direccion: string;
  correo: string;
  telefono: string;
  empresa: string;
  id_usuario?: number;
  estado?: number;
  created?: Date;
}
