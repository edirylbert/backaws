export interface Producto64 {
  id?: number;
  nombre: string;
  descripcion: string;
  cantidadMax: number;
  cantidadMin: number;
  stock: number;
  imagen?: string;
  fileImagen64?: string;
  presentaciones: string;
}
