export interface Proveedor {
  id?: number;
  nombre: string;
  apellido: string;
  direccion: string;
  correo: string;
  telefono: string;
  idUsuario?: number;
  productos?: string;
}
