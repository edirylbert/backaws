export interface Producto {
  id?: number;
  nombre: string;
  descripcion: string;
  cantidadMax: number;
  cantidadMin: number;
  stock: number;
  imagen?: string;
  presentaciones: string;
}
