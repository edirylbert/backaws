export interface Pedido {
  id?: number;
  total: number;
  direccion: string;
  delivery: boolean;
  nombres: string;
  apellidos: string;
  tipoPedido: string;
  telefono: string;
  itemsPedidos?: string;
}
