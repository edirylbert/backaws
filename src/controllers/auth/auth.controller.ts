import { Request, Response } from "express";
import { Usuario } from "../../interfaces/usuarios.interface";
import { encripPassword, comparePassword } from "../../utils/bcrypt";
import { tokenJWT } from ".././../utils/jwt";
import authService from "../../services/auth.service";
import { decodedToken } from "../../utils/jwt";

const authSrv = new authService();

export const register = async (req: Request, res: Response) => {
  try {
    const newUsuario: Usuario = req.body;

    newUsuario.password = await encripPassword(newUsuario.password);
    const response = await authSrv.createUsuario(newUsuario);

    if (response.status !== 0) return res.json(response);

    const token = tokenJWT({ id: response.data.id });
    response.token = token;

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ status: 1, msj: error });
  }
};
export const login = async (req: Request, res: Response) => {
  try {
    const { username, password } = req.body;

    const usernameDB = await authSrv.getUsuarioForEmail(username);

    if (!usernameDB)
      return res.json({ status: 1, msj: "usuario o contraseña incorrecta" });

    const matchPassword = await comparePassword(password, usernameDB.password);

    if (!matchPassword)
      return res.json({
        status: 1,
        msj: "usuario o contraseña incorrecta",
      });

    const token = tokenJWT({ id: usernameDB.id });

    return res.json({
      status: 0,
      msj: `Bienvenido ${usernameDB.username}`,
      token: token,
    });
  } catch (error) {
    return res.status(500).json({ error });
  }
};

export const verificarToken = async (req: Request, res: Response) => {
  try {
    const token = req.headers["x-access-token"];
    if (!token) return res.json({ status: 1, msj: "ingrese token" });
    const decoded: any = decodedToken(token);

    return res.json({
      status: 0,
      msj: "token valido",
      token: token,
    });
  } catch (error) {
    return res.json({ status: 1, msj: error });
  }
};
