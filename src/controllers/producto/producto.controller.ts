import { Request, Response } from "express";
import ProductoService from "../../services/producto.service";
import { Producto } from "../../interfaces/producto.interface";
import { Producto64 } from "../../interfaces/producto64.interface";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import path from "path";
import { global } from "../../config/constant";

const __productoSrv = new ProductoService();

export const getProductos = async (req: Request, res: Response) => {
  try {
    const result = await __productoSrv.getProductos();
    return res.status(200).json({
      status: 0,
      msj: "getProductos",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const getProducto = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await __productoSrv.getProducto(Number(id));

    return res.status(200).json({
      status: 0,
      msj: "getProducto",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const createtProducto = async (req: Request, res: Response) => {
  try {
    const producto: Producto = req.body;

    // producto.imagen = req.file.filename;
    producto.imagen = "";

    const response = await __productoSrv.createProducto(producto);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const editProducto = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const producto: Producto = req.body;

    producto.id = Number(id);
    //producto.imagen = req.file.path;
    producto.imagen = "";

    const response = await __productoSrv.editProducto(producto);

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const deleteProducto = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const response = await __productoSrv.deleteProducto(Number(id));

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const createProducto = async (req: Request, res: Response) => {
  try {
    const newProducto: Producto64 = req.body;
    const nombreImagen = uuidv4();
    const ruta = path.resolve(
      __dirname,
      global.rutaGetImagenes,
      nombreImagen + ".png"
    );

    let base64Image: string =
      newProducto.fileImagen64?.split(";base64,").pop() || "";

    fs.writeFileSync(ruta, base64Image, { encoding: "base64" });

    newProducto.imagen = nombreImagen + ".png";

    const response = await __productoSrv.createProducto(newProducto);

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deletePresentacionProducto = async (
  req: Request,
  res: Response
) => {
  try {
    const { idproducto, idpresentacion } = req.params;

    const result = await __productoSrv.deletePresentacionProducto(
      Number(idproducto),
      Number(idpresentacion)
    );

    return res.status(200).json({
      status: 0,
      msj: "deletePresentacionProducto",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const getStockBasjoProductos = async (req: Request, res: Response) => {
  try {
    const result = await __productoSrv.bajoStockProducto();

    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
