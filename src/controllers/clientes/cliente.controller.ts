import { Request, Response } from "express";
import ClienteService from "../../services/clientes.service";
import { Cliente } from "../../interfaces/clientes.interface";

const __clienteSrv = new ClienteService();

export const getClientes = async (req: Request, res: Response) => {
  try {
    const result = await __clienteSrv.getClientes();

    return res.status(200).json({
      status: 0,
      msj: "Carga Correcta",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const getCliente = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await __clienteSrv.getCliente(Number(id));

    return res.status(200).json({
      status: 0,
      msj: "Carga Correcta",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const createtCliente = async (req: any, res: Response) => {
  try {
    const cliente: Cliente = req.body;

    cliente.nombres = cliente.nombres.toUpperCase();
    cliente.apellidos = cliente.apellidos.toUpperCase();
    cliente.direccion = cliente.direccion.toUpperCase();
    cliente.empresa = cliente.empresa.toUpperCase();
    cliente.id_usuario = Number(req.usuarioID);

    const response = await __clienteSrv.createCliente(cliente);

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const editCliente = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const cliente: Cliente = req.body;

    cliente.id = Number(id);
    cliente.nombres = cliente.nombres.toUpperCase();
    cliente.apellidos = cliente.apellidos.toUpperCase();
    cliente.direccion = cliente.direccion.toUpperCase();
    cliente.empresa = cliente.empresa.toUpperCase();

    const resul = await __clienteSrv.editCliente(cliente);

    return res.status(200).json(resul);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deleteCliente = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const resul = await __clienteSrv.deleteCliente(Number(id));

    return res.status(200).json(resul);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
