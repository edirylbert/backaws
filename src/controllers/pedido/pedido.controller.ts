import { Request, response, Response } from "express";
import PedidoService from "../../services/pedido.service";
import { Pedido } from "../../interfaces/pedido.interface";

const __pedidoSvr = new PedidoService();
export const getPedidos = async (req: Request, res: Response) => {
  try {
    const result = await __pedidoSvr.getPedidos();

    return res.status(200).json({
      status: 0,
      msj: "getPedidos",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const getPedido = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const result = await __pedidoSvr.getPedido(Number(id));

    return res.status(200).json({
      status: 0,
      msj: "getPedido",
      data: result,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const createPedido = async (req: Request, res: Response) => {
  try {
    const pedido: Pedido = req.body;

    const result = await __pedidoSvr.createPedido(pedido);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const editPedido = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const pedido: Pedido = req.body;
    pedido.id = Number(id);

    const result = await __pedidoSvr.editPedido();
    return res.status(200).json({
      status: 0,
      msj: "editPedido",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deletePedido = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await __pedidoSvr.deletePedido();
    return res.status(200).json({
      status: 0,
      msj: "deletePedido",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
export const deleteProductoPedido = async (req: Request, res: Response) => {
  try {
    const { idpedido, idproducto, idpresentacion } = req.params;

    const resul = await __pedidoSvr.deleteProductoPedido(
      Number(idproducto),
      Number(idpedido),
      Number(idpresentacion)
    );
    return res.status(200).json({
      status: 0,
      msj: "deletePedido",
      data: resul,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const despacharPedido = async (req: Request, res: Response) => {
  try {
    const { idPedido } = req.params;

    const result = await __pedidoSvr.despacharPedido(Number(idPedido));
    return res.status(200).json(result);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error });
  }
};

export const pedidosPendientes = async (req: Request, res: Response) => {
  try {
    const result = await __pedidoSvr.pedidosPendientes();
    res.status(200).json({
      status: 0,
      msj: "Pedidos Pendientes",
      data: Number(result),
    });
  } catch (error) {
    return res.status(500).json({ error });
  }
};

export const pedidosDespachados = async (req: Request, res: Response) => {
  try {
    const result = await __pedidoSvr.pedidosDespachados();
    res.status(200).json({
      status: 0,
      msj: "Pedidos despachados",
      data: Number(result),
    });
  } catch (error) {
    return res.status(500).json({ error });
  }
};

export const pedidosRoturaStock = async (req: Request, res: Response) => {
  try {
    const result = await __pedidoSvr.pedidosRoturaStock();
    res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({ error });
  }
};
