import { Response, Request } from "express";
import path from "path";
import fs from "fs";
import { global } from "../../config/constant";

export const getImagenesSlider = async (req: Request, resp: Response) => {
  try {
    const ruta = path.resolve(__dirname, global.rutaGetImagenes);
    const files = await fs.promises.readdir(ruta);
    if (files.length === 0) return resp.json({ status: 0, data: [] });
    return resp.json({ status: 1, data: files });
  } catch (error) {
    return resp.json(error);
  }
};

export const getImagen = async (req: Request, resp: Response) => {
  try {
    const { imagen } = req.params;

    const ruta = path.resolve(__dirname, global.rutaGetImagenes, imagen);

    return resp.sendFile(ruta);
  } catch (error) {
    return resp.json(error);
  }
};
