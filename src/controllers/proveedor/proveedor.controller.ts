import { Request, Response } from "express";
import ProveedorService from "../../services/proveedor.service";
import { Proveedor } from "../../interfaces/proveedor.interface";
const __proveedorSvr = new ProveedorService();
export const getProveedores = async (req: Request, res: Response) => {
  try {
    const result = await __proveedorSvr.getProveedores();
    return res.status(200).json({
      status: 0,
      msj: "getProveedores",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const getProveedor = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await __proveedorSvr.getProveedor(Number(id));
    return res.status(200).json({
      status: 0,
      msj: "getProveedor",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const createProveedor = async (req: any, res: Response) => {
  try {
    const proveedor: Proveedor = req.body;
    proveedor.idUsuario = req.usuarioID;

    const result = await __proveedorSvr.createProveedor(proveedor);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const editProveedor = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const proveedor: Proveedor = req.body;
    proveedor.id = Number(id);

    const result = await __proveedorSvr.editProveedores(proveedor);
    console.log(result);
    return res.status(200).json(result);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deleteProveedor = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await __proveedorSvr.deleteProveedores(Number(id));
    return res.status(200).json({
      status: 0,
      msj: "deleteProveedor",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deleteProductoProveedor = async (req: Request, res: Response) => {
  try {
    const { idProducto, idProveedor } = req.params;
    const result = await __proveedorSvr.deleteProductoProveedor(
      Number(idProveedor),
      Number(idProducto)
    );
    return res.status(200).json({
      status: 0,
      msj: "deleteProveedor",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
