import { Request, Response } from "express";
import rolService from "../../services/rol.service";
import { Rol } from "../../interfaces/rol.interface";
const rolSvr = new rolService();
export const getRoles = async (req: Request, res: Response) => {
  try {
    const response = await rolSvr.getRoles();
    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};
export const getRol = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const response = await rolSvr.getRole(id);
    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
export const createtRol = async (req: Request, res: Response) => {
  try {
    const rol = req.body;
    const response = await rolSvr.createRol(rol);

    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const editRol = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const rol: Rol = req.body;

    const response = await rolSvr.editRol(id, rol);

    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deleteRol = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const response = await rolSvr.deleteRol(id);

    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
