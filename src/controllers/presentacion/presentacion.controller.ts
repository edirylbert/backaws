import { Request, Response } from "express";
import presentacionService from "../../services/presentaciones.service";
import { Presentacion } from "../../interfaces/presentacion.interface";
const presentacionSvr = new presentacionService();
export const getPresentaciones = async (req: Request, res: Response) => {
  try {
    const result = await presentacionSvr.getPresentaciones();

    return res.status(200).json({
      status: 1,
      msj: "Carga Correcta",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};
export const getPresentacion = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const result = await presentacionSvr.getPresentacion(id);
    return res.status(200).json({
      status: 1,
      msj: "Carga Correcta",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
export const createPresentacion = async (req: Request, res: Response) => {
  try {
    const presentacion: Presentacion = req.body;

    const response = await presentacionSvr.createPresentacion(presentacion);

    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500", error });
  }
};

export const editPresentacion = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const presentacion: Presentacion = req.body;
    presentacion.descripcion = presentacion.descripcion.toUpperCase();

    const response = await presentacionSvr.editPresentacion(id, presentacion);

    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};

export const deletePresentacion = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;

    const response = await presentacionSvr.deletePresentacion(id);
    res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ msj: "internar error 500" });
  }
};
