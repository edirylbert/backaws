import bcrypt from "bcrypt";
const saltRounds = 10;

export const encripPassword = (password: string) => {
  const salt = bcrypt.genSaltSync(saltRounds);
  return bcrypt.hashSync(password, salt);
};

export const comparePassword = async (
  passwordReq: string,
  passwordUser: string
) => {
  return bcrypt.compareSync(passwordReq, passwordUser);
};
