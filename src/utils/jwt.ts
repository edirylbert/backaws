import jwt from "jsonwebtoken";
import { globalJWT } from "../config/constant";

export const tokenJWT = (data: any) => {
  return jwt.sign(data, globalJWT.SECRET, {
    expiresIn: 86400, // 24 hours
  });
};

export const decodedToken = (token: any) => {
  return jwt.verify(token, globalJWT.SECRET);
};
