import { Pool } from "pg";
import { globalDataBase } from "../config/constant";
export const connectBD = new Pool({
  user: globalDataBase.user,
  password: globalDataBase.password,
  host: globalDataBase.host,
  port: globalDataBase.port,
  database: globalDataBase.database,
});
