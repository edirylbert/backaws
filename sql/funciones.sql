CREATE OR REPLACE FUNCTION public.__clientes_registrar_clientes(
	__p_nombres_cliente character varying,
	__p_apellidos_cliente character varying,
	__p_direccion_cliente character varying,
	__p_correo_cliente character varying,
	__p_telefono_cliente character varying,
	__p_empresa_cliente character varying,
	__p_usuario_registra_cliente INTEGER)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_cliente 				INTEGER;
	__v_data_cliente			JSONB; 				
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	INSERT 
		INTO 
		cliente (nom_cliente,
				ape_cliente,
				direc_cliente,
				correo_cliente,
				tel_cliente,
				empresa_cliente,
                estado_cliente,
                id_usuario,
                created_on
				) 
		VALUES (__p_nombres_cliente,
                __p_apellidos_cliente,
                __p_direccion_cliente,
                __p_correo_cliente,
                __p_telefono_cliente,
                __p_empresa_cliente,
                1                  ,  
                __p_usuario_registra_cliente,
                NOW()
				)
	    RETURNING id_cliente INTO __v_id_cliente;
		
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,c.id_cliente,
						'nombres' ,c.nom_cliente,
						'apellidos' , c.ape_cliente,
						'direccion',c.direc_cliente,
						'correo', c.correo_cliente,
						'telefono',c.tel_cliente,
                        'empresa',c.empresa_cliente
						)
		INTO __v_data_cliente
		FROM cliente c WHERE c.id_cliente = __v_id_cliente;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_cliente);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;




CREATE OR REPLACE FUNCTION public.__clientes_actualizar_clientes(
	__p_id_cliente INTEGER,
	__p_nombres_cliente character varying,
	__p_apellidos_cliente character varying,
	__p_direccion_cliente character varying,
	__p_correo_cliente character varying,
	__p_telefono_cliente character varying,
	__p_empresa_cliente character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_cliente			JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE cliente c
		SET nom_cliente     = __p_nombres_cliente,
			ape_cliente     = __p_apellidos_cliente,
			direc_cliente   = __p_direccion_cliente,
			correo_cliente  = __p_correo_cliente,
			tel_cliente	   	= __p_telefono_cliente,
			empresa_cliente	= __p_empresa_cliente
 	 WHERE  c.id_cliente    = __p_id_cliente;
	
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,c.id_cliente,
						'nombres' ,c.nom_cliente,
						'apellidos' , c.ape_cliente,
						'direccion',c.direc_cliente,
						'correo', c.correo_cliente,
						'telefono',c.tel_cliente,
						'empresa',c.empresa_cliente
						)
		INTO __v_data_cliente
		FROM cliente c WHERE c.id_cliente = __p_id_cliente;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_cliente);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.__clientes_delete_cliente(
	__p_id_cliente integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE cliente 
		SET estado_cliente = 0
 	 WHERE  id_cliente     = __p_id_cliente;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__presentaciones_registrar_presentacion(
	__p_descripcion_presentacion character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_presentacion 		INTEGER;
	__v_data_presentacion		JSONB; 				
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	INSERT 
		INTO 
		presentacion    (descrip_presentacion             , created_on , estado_presentacion) 
		VALUES 			(__p_descripcion_presentacion     , NOW()	   ,        1    		)
	    RETURNING id_presentacion INTO __v_id_presentacion;
		
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,p.id_presentacion,
						'descripcion' ,p.descrip_presentacion
						)
		INTO __v_data_presentacion
		FROM presentacion p WHERE p.id_presentacion = __v_id_presentacion;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_presentacion);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__presentaciones_actualizar_presentacion(
	__p_id_presentacion 			INTEGER,
	__p_descripcion_presentacion    character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_presentacion		JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE presentacion p
	 SET descrip_presentacion  = __p_descripcion_presentacion
 	 WHERE  p.id_presentacion  = __p_id_presentacion;
	
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,p.id_presentacion,
						'descripcion' ,p.descrip_presentacion
						)
		INTO __v_data_presentacion
		FROM presentacion p WHERE p.id_presentacion = __p_id_presentacion;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_presentacion);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__presentaciones_delete_presentaciones(
	__p_id_presentacion			integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_rol				JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE presentacion p
	SET estado_presentacion  = 0
 	WHERE  p.id_presentacion  = __p_id_presentacion;
	

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;






CREATE OR REPLACE FUNCTION public.__productos_registrar_productos(
	__p_nombre_producto character varying,
	__p_descripcion_producto character varying,
	__p_cantidadmax_producto integer,
	__p_cantidadmin_producto integer,
	__p_stock_producto integer,
	__p_imagen_producto character varying,
	__p_presentaciones_producto jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_producto 			INTEGER;
	__v_data_producto			JSONB;
	__v_presentacion 			JSONB;
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	INSERT 
		INTO 
		producto (  nom_producto,
                    descrip_producto,
                    canti_max_producto,
                    canti_min_producto,
                    stock,
                    img_producto,
                    created_on,
                    estado_producto
				) 
		VALUES (    __p_nombre_producto,
                    __p_descripcion_producto,
                    __p_cantidadMax_producto,
                    __p_cantidadMin_producto,
                    __p_stock_producto,
                    __p_imagen_producto,
                    NOW(),
                    1
				)
	    RETURNING id_producto INTO __v_id_producto;
		
	FOR __v_presentacion IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_presentaciones_producto)
		LOOP
			INSERT INTO 
				producto_x_presentacion (id_producto, 
										 id_presentacion,		 
										 precio,	
										 created_on 
										)
			VALUES 						( __v_id_producto,
										 ( SELECT CAST (__v_presentacion->'id' AS INTEGER) ),  
										 ( SELECT CAST (__v_presentacion->'precio' AS INTEGER) ),
										  NOW()     			 
										);
		END LOOP;

	SELECT 
		JSONB_BUILD_OBJECT(
						'id'            	,p.id_producto,
						'nommbre'       	,p.nom_producto,
                        'descripcion'   	,p.descrip_producto,
                        'cantidad_max'  	,p.canti_max_producto,
                        'cantidad_min'  	,p.canti_min_producto,
                        'stock'         	,p.stock,
                        'imagen'        	,p.img_producto,
						'presentaciones' 	,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
														'id'		  ,p.id_presentacion,
														'descripcion' ,p.descrip_presentacion,
														'precio'	  ,pxp.precio
													))
												FROM producto_x_presentacion pxp,
													 presentacion p	
												WHERE pxp.id_presentacion = p.id_presentacion
												AND  pxp.id_producto	  = __v_id_producto
											)
						)
		INTO __v_data_producto
		FROM producto p WHERE p.id_producto = __v_id_producto;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_producto);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__productos_actualizar_productos(
	__p_id_producto integer,
	__p_nombre_producto character varying,
	__p_descripcion_producto character varying,
	__p_cantidadmax_producto integer,
	__p_cantidadmin_producto integer,
	__p_stock_producto integer,
	__p_imagen_producto character varying,
	__p_presentaciones_producto jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_producto 			INTEGER;
	__v_data_producto			JSONB; 	
	__v_presentacion 			JSONB;
	__v_precio 					DECIMAL;
	__v_created_on 				TIMESTAMP;
	__v_id_presentacion 		INTEGER;
	__v_aux_precio 				DECIMAL;

BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE producto p
		SET nom_producto    	= __p_nombre_producto,
			descrip_producto    = __p_descripcion_producto,
			canti_max_producto  = __p_cantidadMax_producto,
			canti_min_producto  = __p_cantidadMin_producto,
			stock				= __p_stock_producto,
			img_producto    	= __p_imagen_producto	 
 	 WHERE  p.id_producto       = __p_id_producto;
	 
	 	FOR __v_presentacion IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_presentaciones_producto)
			LOOP

				__v_id_presentacion := ( SELECT CAST (__v_presentacion->'id' AS INTEGER) );
				__v_aux_precio 		:= ( SELECT CAST (__v_presentacion->'precio' AS DECIMAL) );

				SELECT 	pxp.precio,
						pxp.created_on 
					INTO __v_precio,
						 __v_created_on
					FROM producto_x_presentacion pxp
					WHERE pxp.id_presentacion = __v_id_presentacion
					AND   pxp.id_producto     = __p_id_producto;

				IF __v_precio <> __v_aux_precio THEN

				   INSERT INTO 
					historico_precio(id_prodcuto,
									 id_presentacion,
									 precio,
									 created_on
									)
					VALUES 			(__p_id_producto,
									 __v_id_presentacion,
									 __v_precio,
									 __v_created_on
									);
					UPDATE producto_x_presentacion pxp
						SET precio 		= __v_aux_precio,
							created_on 	= NOW()
							WHERE pxp.id_presentacion = __v_id_presentacion
							AND   pxp.id_producto     = __p_id_producto;
				END IF;
			

			END LOOP;

	
	SELECT 
		JSONB_BUILD_OBJECT(
						'id'            	,p.id_producto,
						'nommbre'       	,p.nom_producto,
                        'descripcion'   	,p.descrip_producto,
                        'cantidad_max'  	,p.canti_max_producto,
                        'cantidad_min'  	,p.canti_min_producto,
                        'stock'         	,p.stock,
                        'imagen'        	,p.img_producto,
						'presentaciones' 	,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
																	'id'		  ,p.id_presentacion,
																	'descripcion' ,p.descrip_presentacion,
																	'precio'	  ,pxp.precio
																))
															FROM producto_x_presentacion pxp,
																 presentacion p	
															WHERE pxp.id_presentacion = p.id_presentacion
															AND  pxp.id_producto	  = __p_id_producto
														)
						)
		INTO __v_data_producto
		FROM producto p WHERE p.id_producto = __p_id_producto;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_producto);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;




CREATE OR REPLACE FUNCTION public.__productos_delete_productos(
	__p_id_producto integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE producto
		SET estado_producto = 0
 	 WHERE  id_producto     = __p_id_producto;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__roles_registrar_rol(
	__p_descripcion_rol character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_rol 					INTEGER;
	__v_data_rol				JSONB; 				
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	INSERT 
		INTO 
		rol    (descripcion_rol, estado_rol , created_on) 
		VALUES (__p_descripcion_rol, 1	    , NOW()     )
	    RETURNING id_rol INTO __v_id_rol;
		
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,r.id_rol,
						'descripcion' ,r.descripcion_rol
						)
		INTO __v_data_rol
		FROM rol r WHERE r.id_rol = __v_id_rol;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_rol);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__roles_actualizar_roles(
	__p_id_rol 			integer,
	__p_descripcion_rol character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_rol				JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE rol r
		SET descripcion_rol    = __p_descripcion_rol
 	 WHERE  r.id_rol   		   = __p_id_rol;
	
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID'	 		  ,r.id_rol,
						'descripcion' ,r.descripcion_rol
						)
		INTO __v_data_rol
		FROM rol r  WHERE  r.id_rol = __p_id_rol;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_rol);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;

CREATE OR REPLACE FUNCTION public.__roles_delete_roles(
	__p_id_rol 			integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_rol				JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE rol r
		SET estado_rol    = 0
 	 WHERE  r.id_rol   	  = __p_id_rol;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.__usuarios_registrar_usuarios(
	__p_username_usuario character varying,
	__p_email_usuario character varying,
	__p_password_usuario character varying,
	__p_role_usuario integer[])
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_usuario				INTEGER;
	__v_data_usuario			JSONB;
	__v_rol						INTEGER;
	
BEGIN
	INSERT 
		INTO 
		usuario (username_usuario,
				 password_usuario,
				 email_usuario,
				 created_on,
				 estado_usuario	
				)
		VALUES (__p_username_usuario,
                __p_password_usuario,
                __p_email_usuario,
				NOW(),
				1
				)
		RETURNING id_usuario INTO __v_id_usuario;
	
	foreach __v_rol in array __p_role_usuario loop
		INSERT 
			INTO
				usuario_x_rol (id_usuario    , id_rol , created_on)
			VALUES			  (__v_id_usuario, __v_rol, NOW() );
	end loop;
	
	SELECT
	JSONB_BUILD_OBJECT(
		'id',u.id_usuario,
		'usuario',u.username_usuario,
		'email',u.email_usuario,
		'roles', (SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
							'id',r.id_rol,
							'rol',r.descripcion_rol
						))
				  	FROM usuario_x_rol uxr,
				  		 rol r
				  	WHERE uxr.id_rol    = r.id_rol
				    AND  uxr.id_usuario = __v_id_usuario
				)
	)
	INTO __v_data_usuario
	FROM usuario u 
	WHERE  u.id_usuario = __v_id_usuario;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_usuario);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;
