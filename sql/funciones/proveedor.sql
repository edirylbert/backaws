CREATE OR REPLACE FUNCTION public.__proveedores_registrar_proveedores(
	__p_nombre_proveedor    character varying,
	__p_apellido_proveedor  character varying,
	__p_direccion_proveedor character varying,
	__p_correo_proveedor    character varying,
	__p_telefono_proveedor  character varying,
	__p_id_usuario          INTEGER,
	__p_productos_proveedor jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_proveedor			INTEGER;
	__v_data_proveedor			JSONB;
	__v_id_producto     		INTEGER;
	__v_producto     			JSONB;
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	INSERT 
		INTO 
		proveedor ( nom_proveedor,
                    ape_proveedor,
                    direc_proveedor,
                    correo_proveedor,
                    tel_proveedor,
                    estado_proveedor,
                    id_usuario,
                    created_on
				) 
		VALUES (    __p_nombre_proveedor,
                    __p_apellido_proveedor,
                    __p_direccion_proveedor,
                    __p_correo_proveedor,
                    __p_telefono_proveedor,
                    1,
                    __p_id_usuario,
                    NOW()
				)
	    RETURNING id_proveedor INTO __v_id_proveedor;
		
	FOR __v_producto IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_productos_proveedor)
		LOOP
            __v_id_producto := ( SELECT CAST (__v_producto->'id' AS INTEGER) );
			INSERT INTO 
				proveedor_x_producto    (id_proveedor, 
										 id_producto,	 
										 created_on
										)
			VALUES 						( __v_id_proveedor,
                                          __v_id_producto,
                                          NOW()	 
										);
		END LOOP;

	SELECT 
		JSONB_BUILD_OBJECT(
						'id'            	,p.id_proveedor,
						'nombre'            ,p.nom_proveedor,
						'apellido'       	,p.ape_proveedor,
                        'direccion'     	,p.direc_proveedor,
                        'correo'  	        ,p.correo_proveedor,
                        'telefono'      	,p.tel_proveedor,
						'productos' 	    ,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
														'id'		  ,p.id_producto,
														'nombre     ' ,p. nom_producto	
													))
												FROM proveedor_x_producto pxp,
													 producto p	
												WHERE pxp.id_producto = p.id_producto
												AND  pxp.id_proveedor = __v_id_proveedor
											)
						)
		INTO __v_data_proveedor
		FROM proveedor p WHERE p.id_proveedor = __v_id_proveedor;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_proveedor);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;

CREATE OR REPLACE FUNCTION public.__proveedores_actualizar_proveedores(
	__p_id_proveedor integer,
	__p_nombre_proveedor character varying,
	__p_apellido_proveedor character varying,
	__p_direccion_proveedor character varying,
	__p_correo_proveedor character varying,
	__p_telefono_proveedor character varying,
	__p_productos_proveedor jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_proveedor			INTEGER;
	__v_data_proveedor			JSONB;
	__v_id_producto     		INTEGER;
	__v_producto     			JSONB;
	__v_aux_registro			INTEGER;

	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	UPDATE  
		proveedor SET
			nom_proveedor    = __p_nombre_proveedor,
            ape_proveedor    = __p_apellido_proveedor,
            direc_proveedor  = __p_direccion_proveedor,
            correo_proveedor = __p_correo_proveedor,
            tel_proveedor    = __p_telefono_proveedor
		WHERE id_proveedor   = __p_id_proveedor;

	FOR __v_producto IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_productos_proveedor)
		LOOP
            __v_id_producto := ( SELECT CAST (__v_producto->'id' AS INTEGER) );
			SELECT id_proveedor
				INTO __v_aux_registro
				FROM proveedor_x_producto 
				WHERE id_proveedor = __p_id_proveedor
				AND id_producto = __v_id_producto;

			IF __v_aux_registro IS NULL THEN 
					INSERT 
						INTO proveedor_x_producto
							(id_proveedor,
							 id_producto,
							 created_on
							)
						VALUES(__p_id_proveedor,
							   __v_id_producto,
							  NOW()
							  );
			END IF;

		
		END LOOP;

        
	SELECT 
		JSONB_BUILD_OBJECT(
						'id'            	,p.id_proveedor,
						'nombre'            ,p.nom_proveedor,
						'apellido'       	,p.ape_proveedor,
                        'direccion'     	,p.direc_proveedor,
                        'correo'  	        ,p.correo_proveedor,
                        'telefono'      	,p.tel_proveedor,
						'productos' 	    ,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
														'id'		  ,p.id_producto,
														'nombre     ' ,p. nom_producto	
													))
												FROM proveedor_x_producto pxp,
													 producto p	
												WHERE pxp.id_producto = p.id_producto
												AND  pxp.id_proveedor = __p_id_proveedor
											)
						)
		INTO __v_data_proveedor
		FROM proveedor p WHERE p.id_proveedor = __p_id_proveedor;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_proveedor);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;




CREATE OR REPLACE FUNCTION public.__proveedores_delete_proveedores(
	__p_id_proveedor integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE proveedor 
		SET estado_proveedor = 0
 	 WHERE  id_proveedor     = __p_id_proveedor;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


