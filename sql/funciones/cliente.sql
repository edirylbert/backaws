CREATE OR REPLACE FUNCTION public.__clientes_registrar_clientes(
	__p_nombres_cliente character varying,
	__p_apellidos_cliente character varying,
	__p_direccion_cliente character varying,
	__p_correo_cliente character varying,
	__p_telefono_cliente character varying,
	__p_empresa_cliente character varying,
	__p_usuario_registra_cliente INTEGER)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_cliente 				INTEGER;
	__v_data_cliente			JSONB; 				
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	INSERT 
		INTO 
		cliente (nom_cliente,
				ape_cliente,
				direc_cliente,
				correo_cliente,
				tel_cliente,
				empresa_cliente,
                estado_cliente,
                id_usuario,
                created_on
				) 
		VALUES (__p_nombres_cliente,
                __p_apellidos_cliente,
                __p_direccion_cliente,
                __p_correo_cliente,
                __p_telefono_cliente,
                __p_empresa_cliente,
                1                  ,  
                __p_usuario_registra_cliente,
                NOW()
				)
	    RETURNING id_cliente INTO __v_id_cliente;
		
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID'        , c.id_cliente,
						'nombres'   , c.nom_cliente,
						'apellidos' , c.ape_cliente,
						'direccion' , c.direc_cliente,
						'correo'    , c.correo_cliente,
						'telefono'  , c.tel_cliente,
                        'empresa'   , c.empresa_cliente
						)
		INTO __v_data_cliente
		FROM cliente c WHERE c.id_cliente = __v_id_cliente;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_cliente);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;




CREATE OR REPLACE FUNCTION public.__clientes_actualizar_clientes(
	__p_id_cliente INTEGER,
	__p_nombres_cliente character varying,
	__p_apellidos_cliente character varying,
	__p_direccion_cliente character varying,
	__p_correo_cliente character varying,
	__p_telefono_cliente character varying,
	__p_empresa_cliente character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_cliente			JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE cliente c
		SET nom_cliente     = __p_nombres_cliente,
			ape_cliente     = __p_apellidos_cliente,
			direc_cliente   = __p_direccion_cliente,
			correo_cliente  = __p_correo_cliente,
			tel_cliente	   	= __p_telefono_cliente,
			empresa_cliente	= __p_empresa_cliente
 	 WHERE  c.id_cliente    = __p_id_cliente;
	
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,c.id_cliente,
						'nombres' ,c.nom_cliente,
						'apellidos' , c.ape_cliente,
						'direccion',c.direc_cliente,
						'correo', c.correo_cliente,
						'telefono',c.tel_cliente,
						'empresa',c.empresa_cliente
						)
		INTO __v_data_cliente
		FROM cliente c WHERE c.id_cliente = __p_id_cliente;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_cliente);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.__clientes_delete_cliente(
	__p_id_cliente integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE cliente 
		SET estado_cliente = 0
 	 WHERE  id_cliente     = __p_id_cliente;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


