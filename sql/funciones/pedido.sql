-- FUNCTION: public.__pedidos_registrar_pedidos(numeric, character varying, boolean, character varying, character varying, character varying, character varying, jsonb)

-- DROP FUNCTION public.__pedidos_registrar_pedidos(numeric, character varying, boolean, character varying, character varying, character varying, character varying, jsonb);

CREATE OR REPLACE FUNCTION public.__pedidos_registrar_pedidos(
	__p_total_pedido numeric,
	__p_direccion_pedido character varying,
	__p_delivery_pedido boolean,
	__p_nombre_pedido character varying,
	__p_apellido_pedido character varying,
	__p_tipo_pedido character varying,
	__p_telefono_pedido character varying,
	__p_itemspedidos_pedido jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               		JSONB;
    __v_msj_excep               		TEXT;
	__v_id_pedido						INTEGER;
	__v_data_pedido						JSONB;
	__v_id_producto     				INTEGER;
	__v_cantidad_producto     			INTEGER;
	__v_presentacion_producto   		INTEGER;
	__v_precio_presentacion_producto    DECIMAL;
	__v_item    						JSONB;
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	INSERT 
		INTO 
			pedido ( total_pedido,
					 direc_pedido,
					 delivery_pedido,
					 nom_pedido,
					 ape_pedido,
					 tipo_pedido,
					 tel_pedido,
					 created_on,
					 estado_pedido 
 				   ) 
		VALUES    ( __p_total_pedido,
                    __p_direccion_pedido,
                    __p_delivery_pedido,
                    __p_nombre_pedido,
                    __p_apellido_pedido,
                    __p_tipo_pedido,
                    __p_telefono_pedido,
                    NOW(),
				    0
				)
	    RETURNING id_pedido INTO __v_id_pedido;
		
	FOR __v_item IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_itemsPedidos_pedido)
		LOOP
            __v_id_producto			  		 := ( SELECT CAST (__v_item->'idProducto' AS INTEGER) );
            __v_cantidad_producto 	  		 := ( SELECT CAST (__v_item->'cantidad' AS INTEGER) );
            __v_presentacion_producto 		 := ( SELECT CAST (__v_item->'idPresentacion' AS INTEGER ) );
            __v_precio_presentacion_producto := ( SELECT CAST (__v_item->'precio' AS DECIMAL ) );
			INSERT INTO 
				producto_x_pedido   (	id_producto, 
									  	id_pedido,
									  	cantidad,
									  	id_presentacion ,
									  	precio_presentacion
									)
			VALUES 					( __v_id_producto,
                                      __v_id_pedido,
                                      __v_cantidad_producto,
									  __v_presentacion_producto ,
									  __v_precio_presentacion_producto
									);
		END LOOP;

	SELECT 
		JSON_BUILD_OBJECT(
			'id' 			,p.id_pedido,
			'total' 		,p.total_pedido,
			'direccion' 	,p.direc_pedido,
			'delivery'		,p.delivery_pedido,
			'cliente'		,CONCAT (p.nom_pedido,' ', p.ape_pedido),
			'tipo'			,p.tipo_pedido,
			'telefono'		,p.tel_pedido,
			'fecha'			,p.created_on
		)
		INTO __v_data_pedido
		FROM pedido p WHERE p.id_pedido = __v_id_pedido;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_pedido);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;

ALTER FUNCTION public.__pedidos_registrar_pedidos(numeric, character varying, boolean, character varying, character varying, character varying, character varying, jsonb)
    OWNER TO postgres;


CREATE OR REPLACE FUNCTION public.__pedidos_despachar_pedidos(
	__p_id_pedido numeric
    )
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               		JSONB;
    __v_msj_excep               		TEXT;
	__v_json_items_pedido 				JSONB;
	__v_item    			 			JSONB;
	__v_stock    	     				INTEGER;
	__v_id_producto     				INTEGER;
	__v_cantidad_producto     			INTEGER;
	__v_id_presentacion_producto   		INTEGER;

BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE pedido 
		SET estado_pedido = 1
		WHERE id_pedido = __p_id_pedido;

	SELECT JSONB_AGG(
			JSONB_BUILD_OBJECT(
				'idproducto'      ,pro.id_producto,
				'nombre' 		  ,pro.nom_producto,
				'cantidad'	      ,pxp.cantidad,
				'presentacion'	  ,pre.descrip_presentacion,
				'idpresentacion'  ,pxp.id_presentacion
			))
		INTO  __v_json_items_pedido
		FROM  producto_x_pedido pxp,
			  producto pro,
			  presentacion pre
		WHERE pxp.id_pedido = __p_id_pedido
		AND   pxp.id_producto = pro.id_producto
		AND   pxp.id_presentacion = pre.id_presentacion;

	FOR __v_item IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__v_json_items_pedido)
		LOOP
			__v_id_producto			  		 := ( SELECT CAST (__v_item->'idproducto' AS INTEGER) );
			__v_cantidad_producto 	  		 := ( SELECT CAST (__v_item->'cantidad' AS INTEGER) );
			__v_id_presentacion_producto     := ( SELECT CAST (__v_item->'idpresentacion' AS INTEGER ) );
		
			SELECT pxp.stock
				INTO __v_stock
				FROM producto_x_presentacion pxp 
				WHERE pxp.id_producto     = __v_id_producto
				AND   pxp.id_presentacion = __v_id_presentacion_producto;
			
			UPDATE producto_x_presentacion 
				SET stock 			  = (SELECT SUM( __v_stock - __v_cantidad_producto))
				WHERE id_producto     = __v_id_producto
				AND   id_presentacion = __v_id_presentacion_producto;
			
		END LOOP;
		
	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Despacho correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.__pedidos_rotura_stock_pedidos()
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               		JSONB;
    __v_msj_excep               		TEXT;
	__v_json_pedidos_pendientes 		JSONB;
	__v_item_pedido						JSONB;
	__v_id_pedido						INTEGER;
	__v_json_producto_x_pedido 			JSONB;
	__v_aux_stock 						INTEGER;
	__v_item_producto_presentacion 		JSONB;
	__v_aux_cantidad					INTEGER;
	__v_jsonb_respuesta					JSONB DEFAULT '[]'::JSONB ;
	__v_aux_respu						JSONB;
	__v_aux_id_producto					INTEGER;
	__v_aux_id_presentacion				INTEGER;

BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	  SELECT JSONB_AGG(
				JSONB_BUILD_OBJECT(
					'id_pedido'      ,p.id_pedido,
					'nombre'    	 ,p.nom_pedido,
					'apellido'		 ,p.ape_pedido
				))
		INTO __v_json_pedidos_pendientes
		FROM  pedido p
		WHERE p.estado_pedido = 0;
		
		FOR __v_item_pedido IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__v_json_pedidos_pendientes)
			LOOP
				__v_id_pedido := ( SELECT CAST (__v_item_pedido->'id_pedido' AS INTEGER) );
				
				SELECT JSONB_AGG(
							JSONB_BUILD_OBJECT(
								'id_producto'		,pxp.id_producto,
								'id_presentacion'	,pxp.id_presentacion,
								'cantidad'			,pxp.cantidad
							))
					INTO __v_json_producto_x_pedido
					FROM producto_x_pedido pxp
					WHERE pxp.id_pedido = __v_id_pedido ;
				
				FOR __v_item_producto_presentacion IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__v_json_producto_x_pedido)
					LOOP
						__v_aux_cantidad := ( SELECT CAST (__v_item_producto_presentacion->'cantidad' AS INTEGER) );
						__v_aux_id_producto :=( SELECT CAST (__v_item_producto_presentacion->'id_producto' AS INTEGER) );
						__v_aux_id_presentacion := ( SELECT CAST (__v_item_producto_presentacion->'id_presentacion' AS INTEGER) );
						SELECT stock
							INTO __v_aux_stock
							FROM producto_x_presentacion 
							WHERE id_producto = __v_aux_id_producto
							AND   id_presentacion = __v_aux_id_presentacion;
					
						IF __v_aux_cantidad >= __v_aux_stock  THEN 
							SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
												'nombre'		,(SELECT 
																  	nom_producto 
																  	FROM producto 
																  	WHERE id_producto =__v_aux_id_producto ),
												'presentacion'  ,(SELECT 
																  	descrip_presentacion 
																  	FROM presentacion 
																    WHERE id_presentacion = __v_aux_id_presentacion),
												'clientenombre'	  ,__v_item_pedido->'nombre',
												'clienteApellido' ,__v_item_pedido->'apellido'
								))
							INTO __v_aux_respu;
							
							__v_jsonb_respuesta := (SELECT COALESCE ( __v_jsonb_respuesta || __v_aux_respu ));
							
						END IF;
						
					
					END LOOP;
				
			END LOOP;	
		
		
	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Pedidos con rotura stock','data',__v_jsonb_respuesta);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;
