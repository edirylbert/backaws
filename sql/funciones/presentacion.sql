CREATE OR REPLACE FUNCTION public.__presentaciones_registrar_presentacion(
	__p_descripcion_presentacion character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_presentacion 		INTEGER;
	__v_data_presentacion		JSONB; 				
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	INSERT 
		INTO 
		presentacion    (descrip_presentacion             , created_on , estado_presentacion) 
		VALUES 			(__p_descripcion_presentacion     , NOW()	   ,        1    		)
	    RETURNING id_presentacion INTO __v_id_presentacion;
		
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,p.id_presentacion,
						'descripcion' ,p.descrip_presentacion
						)
		INTO __v_data_presentacion
		FROM presentacion p WHERE p.id_presentacion = __v_id_presentacion;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_presentacion);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__presentaciones_actualizar_presentacion(
	__p_id_presentacion 			INTEGER,
	__p_descripcion_presentacion    character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_presentacion		JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE presentacion p
	 SET descrip_presentacion  = __p_descripcion_presentacion
 	 WHERE  p.id_presentacion  = __p_id_presentacion;
	
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,p.id_presentacion,
						'descripcion' ,p.descrip_presentacion
						)
		INTO __v_data_presentacion
		FROM presentacion p WHERE p.id_presentacion = __p_id_presentacion;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_presentacion);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__presentaciones_delete_presentaciones(
	__p_id_presentacion			integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_rol				JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE presentacion p
	SET estado_presentacion  = 0
 	WHERE  p.id_presentacion  = __p_id_presentacion;
	

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;

