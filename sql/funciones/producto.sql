
CREATE OR REPLACE FUNCTION public.__productos_registrar_productos(
	__p_nombre_producto character varying,
	__p_descripcion_producto character varying,
	__p_cantidadmax_producto integer,
	__p_cantidadmin_producto integer,
	__p_stock_producto integer,
	__p_imagen_producto character varying,
	__p_presentaciones_producto jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_producto 			INTEGER;
	__v_data_producto			JSONB;
	__v_presentacion 			JSONB;
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	INSERT 
		INTO 
		producto (  nom_producto,
                    descrip_producto,
                    canti_max_producto,
                    canti_min_producto,
                    stock,
                    img_producto,
                    created_on,
                    estado_producto
				) 
		VALUES (    __p_nombre_producto,
                    __p_descripcion_producto,
                    __p_cantidadMax_producto,
                    __p_cantidadMin_producto,
                    __p_stock_producto,
                    __p_imagen_producto,
                    NOW(),
                    1
				)
	    RETURNING id_producto INTO __v_id_producto;
		
	FOR __v_presentacion IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_presentaciones_producto)
		LOOP
			INSERT INTO 
				producto_x_presentacion (id_producto, 
										 id_presentacion,		 
										 precio,	
										 stock,
										 created_on 
										)
			VALUES 						( __v_id_producto,
										 ( SELECT CAST (__v_presentacion->'id' AS INTEGER) ),  
										 ( SELECT CAST (__v_presentacion->'precio' AS DECIMAL) ),
										 ( SELECT CAST (__v_presentacion->'stock' AS INTEGER) ),
										  NOW()     			 
										);
		END LOOP;

	SELECT 
		JSONB_BUILD_OBJECT(
						'id'            	,p.id_producto,
						'nommbre'       	,p.nom_producto,
                        'descripcion'   	,p.descrip_producto,
                        'cantidad_max'  	,p.canti_max_producto,
                        'cantidad_min'  	,p.canti_min_producto,
                        'stock'         	,p.stock,
                        'imagen'        	,p.img_producto,
						'presentaciones' 	,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
														'id'		  ,p.id_presentacion,
														'descripcion' ,p.descrip_presentacion,
														'precio'	  ,pxp.precio
													))
												FROM producto_x_presentacion pxp,
													 presentacion p	
												WHERE pxp.id_presentacion = p.id_presentacion
												AND  pxp.id_producto	  = __v_id_producto
											)
						)
		INTO __v_data_producto
		FROM producto p WHERE p.id_producto = __v_id_producto;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_producto);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;

CREATE OR REPLACE FUNCTION public.__productos_actualizar_productos(
	__p_id_producto integer,
	__p_nombre_producto character varying,
	__p_descripcion_producto character varying,
	__p_cantidadmax_producto integer,
	__p_cantidadmin_producto integer,
	__p_stock_producto integer,
	__p_imagen_producto character varying,
	__p_presentaciones_producto jsonb)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_producto 			INTEGER;
	__v_data_producto			JSONB; 	
	__v_presentacion 			JSONB;
	__v_precio 					DECIMAL;
	__v_stock 					DECIMAL;
	__v_created_on 				TIMESTAMP;
	__v_id_presentacion 		INTEGER;
	__v_aux_precio 				DECIMAL;
	__v_aux_stock 				DECIMAL;

BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE producto p
		SET nom_producto    	= __p_nombre_producto,
			descrip_producto    = __p_descripcion_producto,
			canti_max_producto  = __p_cantidadMax_producto,
			canti_min_producto  = __p_cantidadMin_producto,
			stock				= __p_stock_producto
				 
 	 WHERE  p.id_producto       = __p_id_producto;
	 
	 	FOR __v_presentacion IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__p_presentaciones_producto)
			LOOP

				__v_id_presentacion := ( SELECT CAST (__v_presentacion->'id' AS INTEGER) );
				__v_aux_precio 		:= ( SELECT CAST (__v_presentacion->'precio' AS DECIMAL) );
				__v_aux_stock 		:= ( SELECT CAST (__v_presentacion->'stock' AS INTEGER) );

				SELECT 	pxp.precio,
						pxp.created_on,
						pxp.stock 
					INTO __v_precio,
						 __v_created_on,
						 __v_stock
					FROM producto_x_presentacion pxp
					WHERE pxp.id_presentacion = __v_id_presentacion
					AND   pxp.id_producto     = __p_id_producto;
					
				IF __v_created_on IS NULL THEN 
					INSERT 
						INTO producto_x_presentacion
							(id_producto,
							 id_presentacion,
							 precio,
							 created_on,
							 stock)
						VALUES(__p_id_producto,
							   __v_id_presentacion,
							   __v_aux_precio,
							   NOW(),
							   __v_aux_stock 
							  );
				END IF;

				IF __v_stock <> __v_aux_stock THEN
					UPDATE producto_x_presentacion pxp
						SET stock		= __v_aux_stock
							WHERE pxp.id_presentacion = __v_id_presentacion
							AND   pxp.id_producto     = __p_id_producto;
				END IF ;
				
				IF __v_precio <> __v_aux_precio THEN

				   INSERT INTO 
					historico_precio(id_prodcuto,
									 id_presentacion,
									 precio,
									 created_on
									)
					VALUES 			(__p_id_producto,
									 __v_id_presentacion,
									 __v_precio,
									 __v_created_on
									);
					UPDATE producto_x_presentacion pxp
						SET precio 		= __v_aux_precio
						
							WHERE pxp.id_presentacion = __v_id_presentacion
							AND   pxp.id_producto     = __p_id_producto;
				END IF;
			

			END LOOP;

	
	SELECT 
		JSONB_BUILD_OBJECT(
						'id'            	,p.id_producto,
						'nommbre'       	,p.nom_producto,
                        'descripcion'   	,p.descrip_producto,
                        'cantidad_max'  	,p.canti_max_producto,
                        'cantidad_min'  	,p.canti_min_producto,
                        'stock'         	,p.stock,
                        'imagen'        	,p.img_producto,
						'presentaciones' 	,(SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
																	'id'		  ,p.id_presentacion,
																	'descripcion' ,p.descrip_presentacion,
																	'precio'	  ,pxp.precio
																))
															FROM producto_x_presentacion pxp,
																 presentacion p	
															WHERE pxp.id_presentacion = p.id_presentacion
															AND  pxp.id_producto	  = __p_id_producto
														)
						)
		INTO __v_data_producto
		FROM producto p WHERE p.id_producto = __p_id_producto;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_producto);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;




CREATE OR REPLACE FUNCTION public.__productos_delete_productos(
	__p_id_producto integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE producto
		SET estado_producto = 0
 	 WHERE  id_producto     = __p_id_producto;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;



CREATE OR REPLACE FUNCTION public.__productos_bajo_stock_productos()
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               			JSONB;
    __v_msj_excep               			TEXT;
	__v_json_ids_productos					JSONB;
	__v_json_presentaciones_producto		JSONB;
	
	__v_item_producto    					JSONB;
	__v_item_presentacion    				JSONB;
	
	__v_id_producto							INTEGER;
	__v_stock_minimo_producto				INTEGER;
	__v_stock_presentacion					INTEGER;
	__v_jsonb_respuesta						JSONB DEFAULT '[]'::JSONB ;
	__v_jsonb_detalle_producto				JSONB;
	__v_aux_respu							JSONB;
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido

	SELECT JSONB_AGG(
			JSONB_BUILD_OBJECT(
				'idproducto'      ,p.id_producto,
				'stock_minimo'    ,p.canti_min_producto,
				'nombre'		  ,p.nom_producto	
			))
		INTO  __v_json_ids_productos
		FROM  producto p
		WHERE p.estado_producto = 1;
		
		
	FOR __v_item_producto IN SELECT * FROM JSONB_ARRAY_ELEMENTS(__v_json_ids_productos)
		LOOP
			__v_id_producto	 				:= ( SELECT CAST (__v_item_producto->'idproducto' AS INTEGER) );
			__v_stock_minimo_producto	 	:= ( SELECT CAST (__v_item_producto->'stock_minimo' AS INTEGER) );
			--------------------------------------------------------------------------
			----------OBTENEMOS LAS PRESENTACIONES DEL PRODUCTO Y SU STOCK------------
			--------------------------------------------------------------------------
			SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
							'id'		    ,p.id_presentacion,
							'stock'	        ,pxp.stock,
							'presentacion'  ,p.descrip_presentacion
						))
					INTO __v_json_presentaciones_producto
					FROM producto_x_presentacion pxp,
						 presentacion p	
					WHERE pxp.id_presentacion = p.id_presentacion
					AND  pxp.id_producto	  = __v_id_producto;
			--------------------------------------------------------------------------
			
			--------------------------------------------------------------------------
			----------RECORREMOS LAS PRESENTACIONES Y VALIDAMOS STOCK	  ------------
			--------------------------------------------------------------------------
	
			FOR __v_item_presentacion IN SELECT JSONB_ARRAY_ELEMENTS(__v_json_presentaciones_producto)
				LOOP
					__v_stock_presentacion	:= ( SELECT CAST (__v_item_presentacion->'stock' AS INTEGER) );
		
					IF __v_stock_presentacion <= __v_stock_minimo_producto THEN
						SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
											'nombre'		,__v_item_producto->'nombre',
											'stock'  	    ,__v_stock_presentacion,
											'presentacion'	,__v_item_presentacion->'presentacion'
							))
						INTO __v_aux_respu;
						--RAISE NOTICE '%', __v_aux_respu;
						
						__v_jsonb_respuesta := (SELECT COALESCE ( __v_jsonb_respuesta || __v_aux_respu ));
						
					END IF;
					
				END LOOP;	
		END LOOP;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Carga correcta','data',__v_jsonb_respuesta);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;



