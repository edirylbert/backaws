CREATE OR REPLACE FUNCTION public.__roles_registrar_rol(
	__p_descripcion_rol character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_rol 					INTEGER;
	__v_data_rol				JSONB; 				
	
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	INSERT 
		INTO 
		rol    (descripcion_rol, estado_rol , created_on) 
		VALUES (__p_descripcion_rol, 1	    , NOW()     )
	    RETURNING id_rol INTO __v_id_rol;
		
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID' ,r.id_rol,
						'descripcion' ,r.descripcion_rol
						)
		INTO __v_data_rol
		FROM rol r WHERE r.id_rol = __v_id_rol;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_rol);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.__roles_actualizar_roles(
	__p_id_rol 			integer,
	__p_descripcion_rol character varying)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_rol				JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE rol r
		SET descripcion_rol    = __p_descripcion_rol
 	 WHERE  r.id_rol   		   = __p_id_rol;
	
	SELECT 
		JSONB_BUILD_OBJECT(
						'ID'	 		  ,r.id_rol,
						'descripcion' ,r.descripcion_rol
						)
		INTO __v_data_rol
		FROM rol r  WHERE  r.id_rol = __p_id_rol;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Actualizo correctamente','data',__v_data_rol);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;

CREATE OR REPLACE FUNCTION public.__roles_delete_roles(
	__p_id_rol 			integer)
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_data_rol				JSONB; 
BEGIN
    ------------------------------------------------------ 0 = no error 
														-- 1 = error controlado // 2 = error desconocido
	UPDATE rol r
		SET estado_rol    = 0
 	 WHERE  r.id_rol   	  = __p_id_rol;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Elimino correctamente');

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;
