
CREATE OR REPLACE FUNCTION public.__usuarios_registrar_usuarios(
	__p_username_usuario character varying,
	__p_email_usuario character varying,
	__p_password_usuario character varying,
	__p_role_usuario integer[])
    RETURNS jsonb
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    __v_respuesta               JSONB;
    __v_msj_excep               TEXT;
	__v_id_usuario				INTEGER;
	__v_data_usuario			JSONB;
	__v_rol						INTEGER;
	
BEGIN
	INSERT 
		INTO 
		usuario (username_usuario,
				 password_usuario,
				 email_usuario,
				 created_on,
				 estado_usuario	
				)
		VALUES (__p_username_usuario,
                __p_password_usuario,
                __p_email_usuario,
				NOW(),
				1
				)
		RETURNING id_usuario INTO __v_id_usuario;
	
	foreach __v_rol in array __p_role_usuario loop
		INSERT 
			INTO
				usuario_x_rol (id_usuario    , id_rol , created_on)
			VALUES			  (__v_id_usuario, __v_rol, NOW() );
	end loop;
	
	SELECT
	JSONB_BUILD_OBJECT(
		'id',u.id_usuario,
		'usuario',u.username_usuario,
		'email',u.email_usuario,
		'roles', (SELECT JSONB_AGG(JSONB_BUILD_OBJECT(
							'id',r.id_rol,
							'rol',r.descripcion_rol
						))
				  	FROM usuario_x_rol uxr,
				  		 rol r
				  	WHERE uxr.id_rol    = r.id_rol
				    AND  uxr.id_usuario = __v_id_usuario
				)
	)
	INTO __v_data_usuario
	FROM usuario u 
	WHERE  u.id_usuario = __v_id_usuario;

	__v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data',__v_data_usuario);

	RETURN __v_respuesta;

	EXCEPTION 
		
		WHEN OTHERS THEN

			GET STACKED DIAGNOSTICS __v_msj_excep = PG_EXCEPTION_CONTEXT;
			__v_respuesta = JSONB_BUILD_OBJECT('status',1,'msj','Hubo un error','stack_error',CONCAT(SQLERRM,' - ',__v_msj_excep));
			RETURN __v_respuesta;
    ------------------------------------------------------
END;
$BODY$;
