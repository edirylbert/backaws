
    do $$
    declare 
        __p_nombre_producto character varying := 'granadilla editado';
        __p_descripcion_producto character varying := 'buena';
        __p_cantidadmax_producto integer:= 100;
        __p_cantidadmin_producto integer:= 20;
        __p_stock_producto integer:= 50;
        __p_imagen_producto character varying:= 'D:\\TESIS\\back\\dist\\public\\uploads\\dbbf1f70-04b9-4664-9ab2-ac46745438d1.jpg';
        __p_descripciones  JSONB := '[{"id":1,"precio":15},{"id":2,"precio":16}]';

    begin

        RAISE NOTICE '%',__p_nombre_producto;
        RAISE NOTICE '%',__p_descripcion_producto;
        RAISE NOTICE '%',__p_cantidadmax_producto;
        RAISE NOTICE '%',__p_cantidadmin_producto;
        RAISE NOTICE '%',__p_stock_producto;
        RAISE NOTICE '%',__p_imagen_producto;
        RAISE NOTICE '%',__p_descripciones;
    
        __v_respuesta = JSONB_BUILD_OBJECT('status',0,'msj','Se Registro correctamente','data','');

        RAISE NOTICE '%',__result;
    EXCEPTION
        WHEN OTHERS THEN
            GET STACKED DIAGNOSTICS __msj_excep = PG_EXCEPTION_CONTEXT;
            __result := JSONB_BUILD_OBJECT('msj'         , 'ERROR_GENERICO',
                                        'status'      , __STATUS_500,
                                        'stack_error' , CONCAT(SQLERRM, ' - ', __msj_excep));
        RAISE NOTICE '%', __result;

        
        
    end $$;



