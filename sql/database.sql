
CREATE TABLE IF NOT EXISTS usuario (
    id_usuario              SERIAL          PRIMARY KEY,
	username_usuario        VARCHAR ( 50 ),
	password_usuario        VARCHAR ( 255 )  NOT NULL,
	email_usuario           VARCHAR ( 50 ) UNIQUE NOT NULL,
	created_on              TIMESTAMP,
    estado_usuario          INTEGER       
);

CREATE TABLE IF NOT EXISTS  rol(
   id_rol           SERIAL        PRIMARY KEY,
   descripcion_rol  VARCHAR (255) UNIQUE NOT NULL,
   created_on       TIMESTAMP,
   estado_rol       INTEGER
);

CREATE TABLE IF NOT EXISTS  usuario_x_rol(
  id_usuario    INT  NOT NULL ,
  id_rol        INT  NOT NULL ,
  created_on    TIMESTAMP,
  PRIMARY KEY   (id_usuario , id_rol),
  FOREIGN KEY   (id_rol)
      REFERENCES rol (id_rol),
  FOREIGN KEY (id_usuario)
      REFERENCES usuario (id_usuario)
);


CREATE TABLE IF NOT EXISTS cliente (
    id_cliente          SERIAL PRIMARY KEY,     
    nom_cliente         VARCHAR (50),
    ape_cliente         VARCHAR (50),
    direc_cliente       VARCHAR (50),
    correo_cliente      VARCHAR (50),
    tel_cliente         VARCHAR (50),
    empresa_cliente     VARCHAR (50),
    estado_cliente      INTEGER ,
    id_usuario          INT NOT NULL,
    created_on          TIMESTAMP,
    FOREIGN KEY         (id_usuario)
        REFERENCES usuario (id_usuario)
);


CREATE TABLE IF NOT EXISTS producto (
    id_producto                     SERIAL PRIMARY KEY,     
    nom_producto                    VARCHAR (50),
    descrip_producto                VARCHAR (250),
    canti_max_producto              INTEGER,
    canti_min_producto              INTEGER,
    stock                           INTEGER ,
    img_producto                    VARCHAR (200),
    created_on                      TIMESTAMP,
    estado_producto                 INTEGER    
);

CREATE TABLE IF NOT EXISTS proveedor (
    id_proveedor          SERIAL PRIMARY KEY,     
    nom_proveedor         VARCHAR (50),
    ape_proveedor         VARCHAR (50),
    direc_proveedor       VARCHAR (50),
    correo_proveedor      VARCHAR (50),
    tel_proveedor         VARCHAR (50),
    estado_proveedor      INTEGER ,
    id_usuario            INT NOT NULL,
    created_on            TIMESTAMP,
    FOREIGN KEY         (id_usuario)
        REFERENCES usuario (id_usuario)
   
);



CREATE TABLE IF NOT EXISTS proveedor_x_producto (
    id_proveedor          INT  NOT NULL,     
    id_producto           INT  NOT NULL,     
    created_on            TIMESTAMP,
    PRIMARY KEY         (id_proveedor , id_producto),
    FOREIGN KEY         (id_proveedor)
        REFERENCES proveedor (id_proveedor),
    FOREIGN KEY         (id_producto)
        REFERENCES producto (id_producto)
);


CREATE TABLE IF NOT EXISTS presentacion (
    id_presentacion         SERIAL PRIMARY KEY,     
    descrip_presentacion    VARCHAR(50),
    created_on              TIMESTAMP,
    estado_presentacion     INTEGER    
);



CREATE TABLE IF NOT EXISTS producto_x_presentacion (
    id_producto         INT   NOT NULL,
    id_presentacion     INT   NOT NULL,
    precio              DECIMAL,
    created_on          TIMESTAMP,  
    stock               INTEGER,
    PRIMARY KEY (id_producto , id_presentacion),
    FOREIGN KEY (id_producto)
        REFERENCES producto (id_producto),
    FOREIGN KEY (id_presentacion)
        REFERENCES presentacion (id_presentacion)
);



CREATE TABLE IF NOT EXISTS historico_precio(
    id_historico_precio             SERIAL PRIMARY KEY,
    id_prodcuto                     INT NOT NULL,
    id_presentacion                 INT NOT NULL,
    precio                          DECIMAL,
    created_on                      TIMESTAMP,
  
    FOREIGN KEY     (id_prodcuto)
        REFERENCES producto (id_producto),
    FOREIGN KEY     (id_presentacion)
        REFERENCES presentacion (id_presentacion)
);

CREATE TABLE IF NOT EXISTS pedido (
    id_pedido           SERIAL PRIMARY KEY,
    total_pedido        DECIMAL,
    nom_pedido          VARCHAR(50),
    ape_pedido          VARCHAR(50),
    direc_pedido        VARCHAR(50),
    delivery_pedido     BOOLEAN,
    tipo_pedido         VARCHAR(50),
    tel_pedido          VARCHAR(50),
    estado_pedido       INTEGER,
    created_on          TIMESTAMP

);

CREATE TABLE IF NOT EXISTS producto_x_pedido (
    id_producto         INT  NOT NULL,
    id_pedido           INT  NOT NULL,
    id_presentacion     INT  NOT NULL,
    cantidad            INTEGER,
    precio_presentacion DECIMAL,
    correlativo         INTEGER,
    PRIMARY KEY (id_producto , id_pedido ,id_presentacion ),
    FOREIGN KEY (id_producto)
        REFERENCES producto (id_producto),
    FOREIGN KEY (id_pedido)
        REFERENCES pedido (id_pedido),
    FOREIGN KEY (id_presentacion)
        REFERENCES presentacion (id_presentacion)
);

CREATE TABLE IF NOT EXISTS comprobante_pago (
    id_comprobante_pago             SERIAL PRIMARY KEY,
    nro_serie_comprobante_pago      VARCHAR(50),
    nro_comprobante_pago            VARCHAR(50),
    tipo_comprobante_pago           VARCHAR(50),
    fecha_emision_comprobante_pago  TIMESTAMP,
    id_pedido                       INT NOT NULL,
    FOREIGN KEY (id_pedido)
        REFERENCES pedido (id_pedido)
);



CREATE TABLE IF NOT EXISTS tipo_pago (
    id_tipo_pago        SERIAL PRIMARY KEY,
    descripcion         VARCHAR(50),
    created_on          TIMESTAMP
)

